import Foundation


enum Engine {
    case StartTheEngine
    case StopTheEngine
}

enum Window {
    case OpenTheWidows
    case CloseTheWindows
}

enum UpdateVolume {
    case Boot
    case Unload
}

struct SmallCar: Hashable {
    let carBrand: String
    let carYear: Int
    let carTrunkVolume: Int
    var engineRunning: Bool
    var windowsOpened: Bool
    var loadedTrunkVolume: Int
    
    init(carBrand: String, carYear: Int, carTrunkVolume: Int) {
        self.carBrand = carBrand
        self.carYear = carYear
        self.carTrunkVolume = carTrunkVolume
        self.engineRunning = false
        self.windowsOpened = false
        self.loadedTrunkVolume = 0
    }
    
    mutating func changeEngineState(state: Engine) {
        switch state {
        case .StartTheEngine:
            self.engineRunning = true
        case .StopTheEngine:
            self.engineRunning = false
        }
    }
    
    mutating func changeWindowState(state: Window) {
        switch state {
        case .OpenTheWidows:
            self.windowsOpened = true
        case .CloseTheWindows:
            self.windowsOpened = false
        }
    }
    
    mutating func changeVolume(state: UpdateVolume) {
        switch state {
        case .Boot:
            if self.loadedTrunkVolume < self.carTrunkVolume {
                self.loadedTrunkVolume += 1
            }
        case .Unload:
            if self.loadedTrunkVolume > 1 {
                self.loadedTrunkVolume -= 1
            }
        }
    }
}

struct Truck: Hashable {
    let carBrand: String
    let carYear: Int
    let carTrunkVolume: Int
    var engineRunning: Bool
    var windowsOpened: Bool
    var loadedTrunkVolume: Int
    
    init(carBrand: String, carYear: Int, carTrunkVolume: Int) {
        self.carBrand = carBrand
        self.carYear = carYear
        self.carTrunkVolume = carTrunkVolume
        self.engineRunning = false
        self.windowsOpened = false
        self.loadedTrunkVolume = 0
    }
    
    mutating func changeEngineState(state: Engine) {
        switch state {
        case .StartTheEngine:
            self.engineRunning = true
        case .StopTheEngine:
            self.engineRunning = false
        }
    }
    
    mutating func changeWindowState(state: Window) {
        switch state {
        case .OpenTheWidows:
            self.windowsOpened = true
        case .CloseTheWindows:
            self.windowsOpened = false
        }
    }
    
    mutating func changeVolume(state: UpdateVolume) {
        switch state {
        case .Boot:
            if self.loadedTrunkVolume < self.carTrunkVolume {
                self.loadedTrunkVolume += 1
            }
        case .Unload:
            if self.loadedTrunkVolume > 1 {
                self.loadedTrunkVolume -= 1
            }
        }
    }
}

var firstCar: SmallCar = SmallCar(carBrand: "LadaLargus", carYear: 2017, carTrunkVolume: 5)
var secondCar: SmallCar = SmallCar(carBrand: "Moskvich", carYear: 1985, carTrunkVolume: 1)
var thirdCar: Truck = Truck(carBrand: "KAMAZ", carYear: 2007, carTrunkVolume: 15)

firstCar.changeWindowState(state: .OpenTheWidows)
firstCar.changeEngineState(state: .StartTheEngine)
secondCar.changeEngineState(state: .StartTheEngine)
secondCar.changeVolume(state: .Boot)
thirdCar.changeWindowState(state: .OpenTheWidows)
thirdCar.changeVolume(state: .Boot)
thirdCar.changeVolume(state: .Boot)
thirdCar.changeVolume(state: .Boot)

var dict: [AnyHashable: String] = [
    firstCar: firstCar.carBrand,
    secondCar: firstCar.carBrand,
    thirdCar: firstCar.carBrand
]

thirdCar.changeVolume(state: .Boot)

for (key, _) in dict {
    print(key)
}

print()

/*------------------------------------------*/

var currentValue = 0

let closure = {
    [currentValue] in
    print("Current value is \(currentValue)")
}

currentValue = 10
closure()

// Capture List позволяет захватывать переменные из окружающего контекста.
// Этот механизм позваляет избежать утечек памяти из-за деаллокации объектов
// или вознекновения цикла сильных ссылок, который не даст удалить объекты.

class Car {
    
    weak var driver: Man?
    
    deinit{
    
    print("машина удалена из памяти")
        
    }
}

class Man {
    
    var myCar: Car?
    
    deinit{
        print("мужчина удален из памяти")
        
    }
    
}
var car: Car? = Car()
var man: Man? = Man()

car?.driver = man
man?.myCar = car

car = nil
man = nil

print()

import Foundation

/*------------------------------------------*/

class Man1 {
    
    var passport: (() -> Passport?)?

    deinit {
        print("мужчина удален из памяти")
    }
}

class Passport {
    let man1: Man1
    init(man1: Man1){
        self.man1 = man1
    }
    deinit {
        print("паспорт удален из памяти")
    }
}

var man1: Man1? = Man1()
var passport: Passport? = Passport(man1: man1!)
man1?.passport = { [weak passport] in
    return passport
}
passport = nil
man = nil